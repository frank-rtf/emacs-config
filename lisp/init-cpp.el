;;; init-cpp.el --- Initialize C++ mode    -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Oleksandr Manenko

;;; Commentary:

;; 

;;; Code:

(require 'init-elpa)
(require-package 'cmake-ide)

(require 'company-clang)
(require 'cmake-ide)

(cmake-ide-setup)

(electric-pair-mode 1)

(setq c-default-style "k&r"
      c-basic-offset 4)

(provide 'init-cpp)
;;; init-cpp.el ends here
